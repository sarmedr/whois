## whois.sh ##
## pre requisites: whois package (apt-get install whois / yum install whois) ##
## usage ##
## setting permission, one off: chmod 744 whois.sh ##
## for ASN lookup: ./whois.sh as1234 ##
## for ip/subnet lookup: ./whois.sh 1.2.3.4 ##

#!/bin/bash
whois $1 | grep -i '(NET-*\|as-name\|org-name\|OrgName\|NetName\|inetnum\|Parent\|No match found'
